-------------------------------------------------------
    Thank you for downloading Soopz Parent Theme!
-------------------------------------------------------
This and any other Theme provided by WiserSites should
be free of bugs, however do note that we're only humans
and if you spot a bug do us a favor and report here

If something goes wrong, or you need our assistance with anything
just register and post your question(s).
